# Guitar Factory

Testing out OOP with basic structures, methods, interfaces, abstract classes etc.

## How it works: 
Creates two different types of guitars which are added to an array, both with their respective data describing  
their features, and type-specific-sounds which are printed in text. 

This is done by creating the two "guitars" through an abstract parent class, as well as their separate sub-types  
through their own classes, Acoustic, and Electric. 

The parent class implements a contract, of a makeSound, which as stated, makes a sound of the type of guitar  
printed out through console. 

*See comments within the code to see respective details*

##About:
An assignment to test out the basics of OOB and the strengths of structuring a project in a specific way.



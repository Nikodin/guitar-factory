package guitars;

//Inherits from Guitars.
public class Acoustic extends Guitars {
    public Acoustic(String brand, String model, int year) {
        super(brand, model, year);
    }

    //Overrides the demanded makeSound, as demanded in the implemented interface of parent.
    @Override
    public void makeSound() {
        System.out.println("*Grabs the Acoustic Guitar...*");
        System.out.println("Bummmmmmmmm... *the warm tone echoes out, and as it does, you lose your pick and -- ");
        System.out.println("Guitar: *Gulp*");

    }

    }


package guitars;

public interface Playable {
    void makeSound();
}

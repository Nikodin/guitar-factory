package guitars;


//Inherits from parent Guitars.
public class Electric extends Guitars {

    //Adds the pickups value to "Electric" extension of guitars.
    private String pickups;

    //Connects to the parent constructor class, along with pickups added for this specific type of guitar.
    public Electric(String brand, String model, int age, String pickups) {
        super(brand, model, age);
        this.pickups = pickups;
    }

    //Overrides the makeSound demanded function as specified in interface.
    @Override
    public void makeSound() {
        System.out.println("*Grabs the Electric Guitar...*");
        System.out.println("*screech* -- *base sound* Duhduh, dah dah dah daa daaa ");
        System.out.println("*you feel an urge within you, to sing Hetfield-style while playing*" );
        System.out.println(" OOOuuuuuuuu, HEEEEEEEEEEEEYYAAAAA OUUUU!");

    }

    }


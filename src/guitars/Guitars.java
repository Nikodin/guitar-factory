package guitars;

//Abstract parent class, implementing Playable interface (see file).
public abstract class Guitars implements Playable {
    private String brand;
    private String model;
    private int year;

    //Constructor for class Guitar.
    public Guitars(String brand, String model, int year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
//Using the makeSound method, as is required with the Interface implemented.
    public abstract void makeSound();

}

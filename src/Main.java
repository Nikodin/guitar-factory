import guitars.Guitars;
import guitars.Acoustic;
import guitars.Electric;

import java.util.ArrayList;


public class Main {
    public static void main (String [] args) {
        //Anything that is attached Guitars will be accessed through the array.
        ArrayList<Guitars> aGuitar = new ArrayList<Guitars>();

        //Constructing two guitars, different types, see the other classes for respective links and input
        //Both added to the Array.
        Acoustic myAcoustic = new Acoustic("Martin", "LX1 Little Martin",2017);
        Electric myElectric = new Electric("ESP", "LTD Signature Iron Cross", 2008, "EMG JH SET");

        //Adds the two types of guitars to the array.
        aGuitar.add(myAcoustic);
        aGuitar.add(myElectric);

        //Loops the contents of the array and their respective makeSound, printing the sound!
        for (Guitars guitars:aGuitar) {
            guitars.makeSound();
        }
    }
}
